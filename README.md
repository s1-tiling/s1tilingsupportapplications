# General

This is an Orfeo Toolbox (https://www.orfeo-toolbox.org/) remote module that
provides support applications for
[S1Tiling chain](https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling).

It is designed to work with OTB >= 7.0

This module can be built as a remote module or as a standalone module

The module provides the following applications:

- MultitempFilteringFilter: that implements Quegan speckle filter for
  SAR Images.
- MultitempFilteringOutcore: that computes the outcore of the filter.

# Getting Started

## Compiling from sources...

### ...Alongside OTB
This module can either be compiled alongside your OTB compilation. To do so,
copy `S1TilingSupportApplications.remote.cmake` into your
`{your_otb_sources_dir}/Modules/Remote` directory.

### ...Independently
Or it could be compiled independently, given you have an OTB
installation that permits the compilation of external modules.

First prepare your environment

```bash
mypath=~/where/i/want/to/work
cd "${mypath}"
mkdir -p S1TilingApps/build
cd S1TilingApps
git clone https://gitlab.orfeo-toolbox.org/s1-tiling/S1TilingSupportApplications
```

And then compile and install

```bash
cd build
cmake ../S1TilingSupportApplications -DOTB_BUILD_MODULE_AS_STANDALONE=ON -DCMAKE_INSTALL_PREFIX=~/where/i/want/to/install/S1TilingSupportApplications -DCMAKE_BUILD_TYPE=<Release or Debug>
make -j 4 && make install
```

#### Lmod bonus
Bonus: if you're using
[lmod](https://lmod.readthedocs.io/en/latest/index.html) `module`, a
modulefile for `S1TilingSupportApplications` will be automatically generated
into `~/where/i/want/to/install/S1TilingSupportApplications/modulefiles`.

If you have loaded your OTB environment through a `module load otb`
before running `cmake`, a dependency toward the actual `otb` module will
be automatically added.

# License

This software is distributed under the Apache License. Please see LICENSE for
details.

# Authors

- Thierry Koleck (CNES)
- Luc Hermitte (CS Group France)
