/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of S1Tiling remote module for Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbMeanFilter.h"
#include "otbMultiplyVectorImageFilter.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbMultiplyVectorImageFilter.h"
#include "otbImageFileWriter.h"
#include "otbMultiToMonoChannelExtractROI.h"

namespace otb
{

namespace Wrapper
{

/**
 * Implements the Quegan speckle filter for SAR images.
 * This application applies the outcore to a list of images. The outcore is
 * genenerated by the `MultitempFilteringOutcore` application.
 *
 * \author Thierry Koleck
 * \copyright CNES
 * \ingroup AppSARUtils
 * \ingroup S1TilingSupportApplications
 */
class MultitempFilteringFilter : public Application
{
public:
  /** Standard class typedefs. */
  using Self                          = MultitempFilteringFilter;
  using MultiplyVectorImageFilterType = typename otb::MultiplyVectorImageFilter<FloatVectorImageType,FloatVectorImageType,FloatVectorImageType>;
  using WriterType                    = otb::ImageFileWriter<FloatVectorImageType>;
  using ExtractChannelFilterType      = otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
          FloatVectorImageType::InternalPixelType>;
  using MeanFilterType = MeanFilter<FloatVectorImageType,FloatVectorImageType>;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(MultitempFilteringFilter, unused);

private:
  void DoInit() override
  {
    SetName("MultitempFilteringFilter");
    SetDescription("This application implements the Quegan speckle filter for SAR images.");

    // Documentation
    SetDocLongDescription("This application implements the Quegan speckle filter for SAR images. It applies the outcore to a list of images. The outcore is genenerated by the MultitempFilteringOutcore application" );


    SetDocLimitations("None");
    SetDocAuthors("Thierry Koleck (CNES)");
    SetDocSeeAlso("MultitempFilteringOutcore");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_InputImageList,  "inl",   "Input images list");
    SetParameterDescription("inl", "Input image list");

    AddParameter(ParameterType_Int ,  "wr",   "Spatial averaging Window radius ");
    SetParameterDescription("wr", "Window radius");

    AddParameter(ParameterType_InputImage, "oc",  "Outcore filename");
    SetParameterDescription("oc", "Outcore filename");

    AddParameter(ParameterType_OutputImage, "enl",  "ENL filename");
    SetParameterDescription("enl", "Number of images averaged");

    AddParameter(ParameterType_String, "filtpath", "Directory where filtered files shall be stored.");
    SetParameterDescription("filtpath", "Directory where filtered files shall be stored. Default: alongside the input file, or in a subdirectory named 'filtered' if the input image contains a slash in its name.");
    this->MandatoryOff("filtpath");

    AddRAMParameter();

    SetDocExampleParameterValue("inl", "s1a_47PQQ_vv_DES_062_20180*.tif");
    SetDocExampleParameterValue("wr", "2");
    SetDocExampleParameterValue("oc", "outcore.tif");
    SetDocExampleParameterValue("filtpath", "$TMPDIR/filtered");
    SetDocExampleParameterValue("enl", "enl.tif");
    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    int Radius = this->GetParameterInt("wr");
    FloatVectorImageListType::Pointer inList = this->GetParameterImageList("inl");
    // On verifie que la liste en entree n'est pas vide
    // std::cout << inList->Size() << "\n";
    if( inList->Size() == 0 )
    {
      itkExceptionMacro("No input Image set...");
    }
    std::string const where = GetParameterString("filtpath");
    std::vector< std::string> filelist=this->GetParameterStringList("inl");

    inList->GetNthElement(0)->UpdateOutputInformation();
    // Get image size

    // Initialise le filtre de calcul du outcore

    FloatVectorImageType::Pointer Outcore = GetParameterFloatVectorImage("oc");

    // Calcul des images de sortie
    m_MultiplyByOutcoreImageFilter = MultiplyVectorImageFilterType::New();
    m_MeanImageFilter              = MeanFilterType::New();

    std::ostringstream oss;
    for( unsigned int i=0; i<inList->Size(); i++ )
    {
      m_MeanImageFilter->SetInput(inList->GetNthElement(i));
      m_MeanImageFilter->SetRadius(Radius);
      m_MeanImageFilter->UpdateOutputInformation();

      m_MultiplyByOutcoreImageFilter->SetInput1(m_MeanImageFilter->GetOutput());
      m_MultiplyByOutcoreImageFilter->SetInput2(Outcore);
      m_MultiplyByOutcoreImageFilter->UpdateOutputInformation();

      // Definit le nom du fichier de sortie (images filtrees)
      auto const& filename = filelist[i];
      auto const  index_slash = filename.find_last_of("/");
      auto const  index_ext   = filename.find_last_of(".");
      auto const& extension   = filename.substr(index_ext);

    auto const& basename  = index_slash == std::string::npos
        ? filename.substr(0,             index_ext)
        : filename.substr(index_slash+1, index_ext-index_slash-1)
        ;
    auto const prefix
        = !where.empty()                   ? (where + "/")
        : index_slash == std::string::npos ? "filtered"
        :                                    filename.substr(0,index_slash)+"/filtered/"
        ;
      oss.str("");
      oss << prefix << basename << "_filtered" << extension;

      // writer label
      WriterType::Pointer writer = WriterType::New();
      writer->SetFileName(oss.str());
      writer->SetInput(m_MultiplyByOutcoreImageFilter->GetOutput());
      AddProcess(writer, writer->GetFileName());
      writer->Update();
    }

    m_Filter = ExtractChannelFilterType::New();
    m_Filter->SetInput(Outcore);
    m_Filter->SetChannel(Outcore->GetVectorLength());
    m_Filter->UpdateOutputInformation();
    SetParameterOutputImage("enl", m_Filter->GetOutput());
    SetParameterOutputImagePixelType("enl",ImagePixelType_uint16);
  }

  MultiplyVectorImageFilterType::Pointer m_MultiplyByOutcoreImageFilter;
  MeanFilterType::Pointer                m_MeanImageFilter;
  ExtractChannelFilterType::Pointer      m_Filter;
};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::MultitempFilteringFilter)
