/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of S1Tiling remote module for Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbOutcoreFilter.h"
#include "otbMultiplyVectorImageFilter.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbObjectList.h"
#include "itkAddImageFilter.h"

namespace otb
{

namespace Wrapper
{

/**
 * Implements the Quegan speckle filter for SAR images.
 * This application computes the outcore function of the filter. It must be
 * followed by the `MultitempFilteringFilter` application to compute the
 * filtered images
 *
 * \author Thierry Koleck
 * \copyright CNES
 * \ingroup AppSARUtils
 * \ingroup S1TilingSupportApplications
 */
class MultitempFilteringOutcore : public Application
{
public:
  /** Standard class typedefs. */
  using Self                          = MultitempFilteringOutcore;
  using AddVectorImageFilterType      = itk::AddImageFilter<FloatVectorImageType,FloatVectorImageType,FloatVectorImageType>;
  using OutcoreFilterType             = OutcoreFilter<FloatVectorImageType,FloatVectorImageType>;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(MultitempFilteringOutcore, otb::Wrapper::Application);

private:
  void DoInit() override
  {
    SetName("MultitempFilteringOutcore");
    SetDescription("This application implements the Quegan speckle filter for SAR images. It computes the outcore function of the filter. It must be followed by the MultitempFilteringFilter application to compute the filtered images");

    // Documentation
    SetDocLongDescription("This application implements the Quegan speckle filter for SAR images. It computes the outcore function of the filter. It must be followed by the MultitempFilteringFilter application to compute the filtered images" );


    SetDocLimitations("None");
    SetDocAuthors("Thierry Koleck (CNES), Marie Ballere (CNES)");
    SetDocSeeAlso("MultitempFilteringFilter");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_InputImageList,  "inl",   "Input images list");
    SetParameterDescription("inl", "Input image list");

    AddParameter(ParameterType_Int ,  "wr",   "Spatial averaging Window radius ");
    SetParameterDescription("wr", "Window radius");

    AddParameter(ParameterType_OutputImage, "oc",  "Outcore filename");
    SetParameterDescription("oc", "Outcore filename");

    AddRAMParameter();

    SetDocExampleParameterValue("inl", "s1a_47PQQ_vv_DES_062_20180*.tif");
    SetDocExampleParameterValue("wr", "2");
    SetDocExampleParameterValue("oc", "outcore.tif");
    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    int Radius = this->GetParameterInt("wr");
    FloatVectorImageListType::Pointer inList = this->GetParameterImageList("inl");
    // On verifie que la liste en entree n'est pas vide
    // std::cout << inList->Size() << "\n";
    if( inList->Size() == 0 )
    {
      itkExceptionMacro("No input Image set...");
    }
    std::vector< std::string> filelist;
    filelist=this->GetParameterStringList("inl");

    inList->GetNthElement(0)->UpdateOutputInformation();

    m_FilterList=otb::ObjectList<itk::ImageToImageFilter<FloatVectorImageType,FloatVectorImageType> >::New();

    AddVectorImageFilterType::Pointer addImageFilter = AddVectorImageFilterType::New();
    OutcoreFilterType::Pointer        outcoreFilter=OutcoreFilterType::New();
    outcoreFilter->SetInput(inList->GetNthElement(0));
    outcoreFilter->SetRadius(Radius);
    outcoreFilter->UpdateOutputInformation();
    m_FilterList->PushBack(outcoreFilter);
    m_FilterList->PushBack(outcoreFilter);

    for( unsigned int i=1; i<inList->Size(); i++ )
    {
      outcoreFilter = OutcoreFilterType::New();
      outcoreFilter->SetInput(inList->GetNthElement(i));
      outcoreFilter->SetRadius(Radius);
      outcoreFilter->UpdateOutputInformation();
      addImageFilter = AddVectorImageFilterType::New();
      addImageFilter->SetInput1(outcoreFilter->GetOutput());
      addImageFilter->SetInput2(m_FilterList->GetNthElement(2*i-1)->GetOutput());
      addImageFilter->UpdateOutputInformation();

      m_FilterList->PushBack(outcoreFilter);
      m_FilterList->PushBack(addImageFilter);
    }

    // Calcul des images de sortie
    SetParameterOutputImage("oc", m_FilterList->Back()->GetOutput());

  }
  otb::ObjectList<itk::ImageToImageFilter<FloatVectorImageType,FloatVectorImageType> >::Pointer m_FilterList;



};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::MultitempFilteringOutcore)
